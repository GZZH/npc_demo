from weapon import Weapon
from potion import Potion
from npc import Npc

import input_core
import copy

def command_list_gen(command_dict):
    return [k for k in command_dict.keys()]

def next_level():
    global level

    level+=1
    print("Level "+ str(level))

def make_potion():
    p = Potion("test potion")
    print(p)

def copy_test():
    t = copy.deepcopy(npc_list[0])
    t.id = '100'
    print(t)
    print(npc_list[0])

def make_weapon():
    global item_list
    w = Weapon("test weapon")
    item_list.append(w)

def make_10_weapon():
    global item_list
    for x in range(10):
        w = Weapon("test weapon in 10")
        item_list.append(w)

def gen_npc():
    n = Npc()
    npc_list.append(n)

def select_item():
    item_dict = {str(item) : item for item in item_list}
    item_command_list =command_list_gen(item_dict)
    return item_dict[input_core.input_collecter(item_command_list)]

def select_npc():
    npc_dict = {str(npc) : npc for npc in npc_list}
    npc_command_list =command_list_gen(npc_dict)
    return npc_dict[input_core.input_collecter(npc_command_list)]

def point_item():
    global pointer
    item_dict = {str(item) : item for item in item_list}
    item_command_list =command_list_gen(item_dict)
    pointer = item_dict[input_collecter(item_command_list)]
    print(pointer)

def point_npc():
    global pointer
    npc_dict = {str(npc) : npc for npc in npc_list}
    npc_command_list =command_list_gen(npc_dict)
    pointer = npc_dict[input_collecter(npc_command_list)]
    print(pointer)
    npc_command_dict = pointer.command
    npc_command_list =command_list_gen(npc_command_dict)

    npc_command_dict[input_collecter(npc_command_list)]()

def npc_gain_item():
    n = select_npc()
    w = select_item()
    n.receive_item(w)
    print(n)

def npc_equip_eqpt():
    n = select_npc()
    w = select_item()
    result = n.equip_eqpt(w)
    '''print(n.equip_eqpt(Weapon(hold_type = "2_hand", position = ("and", "main_hand", "off_hand"))))
    print(n.equip_eqpt(Weapon(hold_type = "off_hand", position = ("or", "off_hand"))))
    print(n.equip_eqpt(Weapon(hold_type = "1_hand", position = ("or", "main_hand", "off_hand"))))
    print(n.equip_eqpt(Weapon(hold_type = "main_hand", position = ("or", "main_hand"))))'''
    print(n)

def npc_swap_eqpt():
    n = select_npc()
    w = select_item()
    result = n.equip_eqpt(w)
    '''print(n.swap_eqpt(Weapon(hold_type = "off_hand", position = ("or", "off_hand"))))
    print(n.swap_eqpt(Weapon(hold_type = "1_hand", position = ("or", "main_hand", "off_hand"))))
    print(n.swap_eqpt(Weapon(hold_type = "main_hand", position = ("or", "main_hand"))))
    print(n.swap_eqpt(Weapon(hold_type = "2_hand", position = ("and", "main_hand", "off_hand"))))'''
    print(n)

def npc_unequip_eqpt():
    n = select_npc()
    pos = n.select_pos()
    result = n.unequip_eqpt("main_hand")
    print(n)

def interact_npc():
    n = select_npc()
    npc_command = n.select_command()
    eval("n."+npc_command)

def auto_setup():
    make_10_weapon()
    gen_npc()

def print_npcs():
    for n in npc_list:
        print(n)

def print_items():
    for i in item_list:
        print(i)

def print_all():
    print_npcs()
    print_items()

def exit():
    global run_flag
    run_flag = False
    print("Game exit")

def console():
    eval(input())

def main():
    command_list = command_list_gen(command_dict)
    while run_flag:
        eval(command_dict[input_core.input_collecter(command_list)])

level = 0
npc_list = []
item_list = []
pointer = None
run_flag = True
player = Npc(name = "player")

command_dict ={
"Next level" : "next_level()",
"Auto setup" : "auto_setup()",
"Make a weapon" : "make_weapon()",
"Make 10 weapon" : "make_10_weapon()",
"Make a potion" : "make_potion()",
"Gen a NPC" : "gen_npc()",
#"Give a NPC item" : npc_gain_item,
#"Give a NPC equipment to equip" : npc_equip_eqpt,
#"Swap a NPC equipment" : npc_swap_eqpt,
#"Unequip a NPC equipment": npc_unequip_eqpt,
"Interact npc" : "interact_npc()",
#"Select npc" : "point_npc()",
"Copy test" : "copy_test()",
"Print npcs" : "print_npcs()",
"Print items" : "print_items()",
"Print All" : "print_all()",
"Open Console" : "console()",
"Exit" : "exit()"}

player_command_dict = {

}
