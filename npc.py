import random_core
import print_core
import input_core

class Npc(object):
    def __init__(self, name = "anonymous", world = None):
        self.id = random_core.random_npc_id()
        self.name = name
        self.world = world
        self.eqpt_pos = ["main_hand", "off_hand", # Weapon
                         "head", "shoulders", "chest", "wrist", "hands", "waist", "legs", "feet", # Armor
                         "neck", "finger1", "finger2", "bag"] # Non-armor
        self.equipments = [None] * len(self.eqpt_pos)
        self.eqpt_pos = {self.eqpt_pos[i] : i for i in range(len(self.eqpt_pos))}
        self.bagpack = []
        self.bagpack_space = 10
        self.command = {"Give item to someone" : self.give_item,
                        "Recieve item" : self.receive_item,
                        "Equip eqpt" : self.equip_eqpt,
                        "Unequip eqpt" : self.unequip_eqpt,
                        "Swap eqpt" : "swap_eqpt(select_item())"}
        self.command_list = list(self.command.keys())

    def select_command(self):
        self.command[input_core.input_collecter(self.command_list)]()

    def remove_item(self, item = None, new_owner = None):
        if item is None:
            item = self.select_item()
        if item is not None and item in self.bagpack:
            self.bagpack.remove(item)
            item.took_by(new_owner)

    def receive_item(self, item = None):
        if item is None:
            item = self.world.select_item()
        if item is not None and (len(self.bagpack) < self.bagpack_space) and item not in self.bagpack:
            self.bagpack.append(item)
            item.took_by(self)
            return True
        else:
            return False

    def give_item(self, n = None, item = None):
        if n is None:
            n = self.world.select_npc()
        if item is None:
            item = self.select_item()
        if n is not None and item is not None and n.receive_item(item):
            self.remove_item(item = item, new_owner = n)

    def select_item(self):
        return input_core.input_collecter(self.bagpack)

    def select_pos(self):
        return self.eqpt_pos[input_core.input_collecter(list(self.eqpt_pos.keys()))]

    def swap_item(self, n):
        pass

    def get_all_itmes(self):
        for item in self.bagpack:
            yield item

    def unequip_eqpt(self, pos = None):
        if pos is None:
            pos = self.select_pos()
        if pos is None:
            return False
        eqpt = self.equipments[pos]
        if eqpt is None:
            return False
        if eqpt.position[0] == "or" and self.receive_item(eqpt):
            self.equipments[pos] = None
        elif eqpt.position[0] == "and" and self.receive_item(eqpt):
            locs = [self.eqpt_pos[i] for i in eqpt.position[1:]]
            for loc in locs:
                self.equipments[loc] = None
        return True

    def equip_eqpt(self, item = None):
        if not item:
            item = self.select_item()
        if not item:
            return False
        if(item.position[0] == "or"):
            locs = [self.eqpt_pos[i] for i in item.position[1:]]
            for loc in locs:
                if(self.equipments[loc] == None):
                    self.equipments[loc] = item
                    self.remove_item(item = item, new_owner = self)
                    return True
        elif(item.position[0] == "and"):
            locs = [self.eqpt_pos[i] for i in item.position[1:]]
            for loc in locs:
                if self.equipments[loc]:
                    return False
            for loc in locs:
                self.equipments[loc] = item
            self.remove_item(item = item, new_owner = self)
            return True
        return False

    def swap_eqpt(self, item = None):
        """
        Not Working
        """
        if self.equip_eqpt(item):
            return None
        else:
            pos = item.position[1:]
            for p in pos:
                cache_item = self.unequip_eqpt(p)
                if cache_item == None:
                    pass
                elif self.equip_eqpt(item):
                    return cache_item
                else:
                    self.equip_eqpt(cache_item)
        return item

    def __str__(self):
        print_core.format_string(["NPC ID: ", self.id])
        print_core.format_string(["Name: ", self.name])

        print_core.skip_line()

        for equip_name, equip in zip(self.eqpt_pos.keys(), self.equipments):
            print_core.format_string([str(equip_name)+": "])
            print_core.add_tab()
            if equip:
                print_core.connect_str(str(equip))
            else:
                print_core.format_string(["Empty"])
            print_core.skip_line()
            print_core.min_tab()

        print_core.skip_line()

        print_core.format_string(["Bagpack: ", str(len(self.bagpack)) + "/" + str(self.bagpack_space)])
        print_core.add_tab()
        for item in self.bagpack:
            print_core.connect_str(str(item))
            print_core.skip_line()
        print_core.min_tab()

        return print_core.pop_formatted_string()
