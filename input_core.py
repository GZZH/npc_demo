def input_collecter(command_list):
    i = 0
    for command in command_list:
        print("%s. %s" % (i, command))
        i += 1

    if i == 0:
        return None
    print("Type in command number")
    inp = None
    try:
        inp = int(input())
    except:
        inp = None

    while inp == None or (inp >= i or inp < 0 or command_list[inp] not in command_list):
        print("Not vaild command number")
        try:
            inp = int(input())
        except:
            inp = None

    return (command_list[inp])
