import time

def random_item_id():
    global last_item_id
    id = int(time.time() * 100000)
    if last_item_id >= id:
        id = last_item_id + 1
    last_item_id = id
    return "0" + str(id)

def random_npc_id():
    global last_npc_id
    id = int(time.time() * 100000)
    if last_npc_id >= id:
        id = last_npc_id + 1
    last_npc_id = id
    return "1" + str(id)

last_item_id = 0
last_npc_id = 0
