import random_core
import print_core

class Item():
    def __init__(self, name = "test item"):
        self.id = random_core.random_item_id()
        self.name = name
        self.type = "item"
        self.stack_able = True
        self.max_stack = 99
        self.stack_num = 1
        self.owner = None

    def took_by(self, n):
        self.owner = n

    def __str__(self):
        print_core.format_string(["Item ID: ", self.id])
        print_core.format_string(["Name: ", self.name])
        owner_name = None
        if self.owner:
            owner_name = self.owner.name
        print_core.format_string(["Owner: ", owner_name])
        print_core.format_string(["Type: ", self.type])
        if(self.stack_able):
            print_core.format_string(["Number: ", str(self.stack_num) + "/" + str(self.max_stack)])

        return print_core.pop_formatted_string()
