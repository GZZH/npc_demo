from world import World

class MasterWorld(World):
    def __init__(self):
        super().__init__()
        """Overwrite variable"""

        """Unique variable"""
        self.run_flag = True
        self.command = {
                        "Next day" : self.next_day,
                        "Auto setup" : self.auto_setup,
                        "Make a weapon" : self.make_weapon,
                        "Make a potion" : self.make_potion,
                        "Gen a NPC" : self.gen_npc,
                        #"Give a NPC item" : self.npc_gain_item,
                        #"Give a NPC equipment to equip" : self.npc_equip_eqpt,
                        #"Swap a NPC equipment" : self.npc_swap_eqpt,
                        #"Unequip a NPC equipment": self.npc_unequip_eqpt,
                        "Interact npc" : self.interact_npc,
                        #"Select npc" : self.point_npc,
                        "Open Console" : self.console,
                        "Exit" : self.exit}
        self.command_list = list(self.command.keys())

    def next_day(self):
        self.day += 1
        print_core.format_string(["Day ", self.day])
        print_core.skip_line()
        print(print_core.pop_formatted_string())

    def main(self):
        while self.run_flag:
            self.select_command()
            self.main()

    def select_command(self):
        self.command[input_core.input_collecter(self.command_list)]()

    def auto_setup(self):
        for x in range(10):
            w = Weapon("random_item_" + str(x))
            self.item_list.append(w)
        self.gen_npc()
        self.gen_npc()
        n = self.npc_list[-1]
        n.receive_item(w)
        n.equip_eqpt(w)

    def gen_npc(self):
        n = Npc(world = self)
        self.npc_list.append(n)

    def make_potion(self):
        p = Potion("test potion")
        self.item_list.append(p)

    def make_weapon(self):
        w = Weapon("test weapon")
        self.item_list.append(w)

    def select_item(self):
        return input_core.input_collecter(self.item_list)

    def select_npc(self):
        return input_core.input_collecter(self.npc_list)

    def interact_npc(self):
        n = self.select_npc()
        npc_command = n.select_command()

    def console(self):
        eval(input())

    def exit(self):
        self.run_flag = False
