from item import Item

class Equipment(Item):
    def __init__(self, name):
        super().__init__(name)
        """Overwrite variable"""
        self.type = "equipment"
        self.max_stack = 1
        self.stack_able = False

        """Unique variable"""
        self.damage = 10
        self.durability = 10
        self.max_durability = 10

    def __str__(self):
        info = super().__str__()
        info += "Damage: \t%d\n"\
                "Durability: \t%d/%d\n"\
                %(self.damage, self.durability, self.max_durability)
        return info
