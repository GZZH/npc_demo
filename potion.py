from item import Item

class Potion(Item):
    def __init__(self, name = "test potion"):
        super().__init__(name)
        """Overwrite variable"""
        self.type = "potion"

        """Unique variable"""
        self.effect = "no effect so far"

    def __str__(self):
        print_core.connect_str(str(super().__str__()))
        print_core.format_string(["Effect: ", self.effect])

        return print_core.pop_formatted_string()
