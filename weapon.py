from item import Item
import print_core

class Weapon(Item):
    def __init__(self,
                 name = "test weapon",
                 hold_type = "1_hand",
                 position = ("or", "main_hand", "off_hand"),
                 damage = 10,
                 max_durability = 10,
                 weapon_type = "sword",
                 material = "metal"):
        super().__init__(name)
        """Overwrite variable"""
        self.type = "weapon"
        self.max_stack = 1
        self.stack_able = False

        """Unique variable"""
        self.hold_type = hold_type
        self.damage = damage
        self.durability = max_durability
        self.max_durability = max_durability
        self.weapon_type = weapon_type
        self.material = material
        self.position = position

    def __str__(self):
        print_core.connect_str(str(super().__str__()))
        cache_pos_str = self.position[1]
        for pos in self.position[2:]:
            cache_pos_str += " " + self.position[0] + " " + pos
        print_core.format_string(["Weapon Type: ", cache_pos_str + " " + str(self.material) + " " + str(self.weapon_type)])
        #print_core.format_string(["Position: ", cache_pos_str])
        print_core.format_string(["Damage: ", self.damage])
        print_core.format_string(["Durability: ", str(self.durability) + "/" + str(self.max_durability)])

        return print_core.pop_formatted_string()
