def format_string(string_list):
    global info, tab_num

    for num in range(tab_num):
        info += " ".ljust(string_len)

    for part in string_list:
        info += str(part).ljust(string_len)
    info += "\n"

def connect_str(lines):
    global info
    info += lines

def pop_formatted_string():
    global info
    cache_info = info
    info = ""
    return cache_info

def skip_line(lines = 1):
    global info
    info += "\n" * lines

def add_tab():
    global tab_num
    tab_num += 1

def min_tab():
    global tab_num
    tab_num -= 1
    if(tab_num < 0):
        tab_num = 0

info = ""
string_len = 20
tab_num = 0
